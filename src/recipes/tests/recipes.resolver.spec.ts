import { Test, TestingModule } from '@nestjs/testing';
import { RecipesResolver } from '../recipes.resolver';
import { RecipesService } from '../recipes.service';
import { IngredientsService } from '../../ingredients/ingredients.service';
import { CategoriesService } from '../../categories/categories.service';
import { getModelToken } from '@nestjs/mongoose';
import { Recipe } from '../recipe.schema';
describe('RecipesResolver', () => {
  let resolver: RecipesResolver;
  let service: RecipesService;
  let ingredientService: IngredientsService;
  let categoryService: CategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecipesResolver, RecipesService,
        {
          provide: getModelToken(Recipe.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: IngredientsService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: CategoriesService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    resolver = module.get<RecipesResolver>(RecipesResolver);
    service = module.get<RecipesService>(RecipesService);
    ingredientService = module.get<IngredientsService>(IngredientsService);
    categoryService = module.get<CategoriesService>(CategoriesService);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
  describe('findAll', () => {
    it('should call findAll method of the RecipesService', async () => {
      const mockRecipes = [{ name: 'Recipe 1', Recipe: 'Recipe 1', instruction: 'instruction'}, { name: 'recipe 2', Recipe: 'Recipe 1', instruction: 'instruction' }];
      const recipeDocument = mockRecipes as any;
      const findAllSpy = jest.spyOn(service, 'findAll').mockResolvedValue(recipeDocument);

      const result = await resolver.findAll();

      expect(result).toEqual(mockRecipes);
      expect(findAllSpy).toHaveBeenCalled();
    });
  });

  describe('findOne', () => {
    it('should call findOne method of the RecipesService', async () => {
      const recipeId = 'someRecipeId';
      const mockRecipe = {_id: recipeId,  name: 'Recipe 1', Recipe: 'Recipe 1', instruction: 'instruction'};

      // Convierte el objeto en un documento Mongoose válido
      const recipeDocument = mockRecipe as any;

      const findOneSpy = jest.spyOn(service, 'findOne').mockResolvedValue(recipeDocument);

      const result = await resolver.findOne(recipeId);

      expect(result).toEqual(recipeDocument);
      expect(findOneSpy).toHaveBeenCalledWith(recipeId);
    });
  });

  describe('category_recipes', () => {
    it('should call findCategoryRecipes method of the RecipesService', async () => {
      const category = 'SomeCategory';
      const mockRecipes = [{ name: 'Recipe 1' }, { name: 'Recipe 2' }];
      const recipeDocument = mockRecipes as any;
      const findCategoryRecipesSpy = jest.spyOn(service, 'findCategoryRecipes').mockResolvedValue(recipeDocument);

      const result = await resolver.category_recipes(category);

      expect(result).toEqual(mockRecipes);
      expect(findCategoryRecipesSpy).toHaveBeenCalledWith(category);
    });
  });

  describe('searchRecipe', () => {
    it('should call searchRecipes method of the RecipesService', async () => {
      const query = 'SearchQuery';
      const mockRecipes = [{ name: 'Recipe 1' }, { name: 'Recipe 2' }];

      const recipeDocument = mockRecipes as any;
      const searchRecipesSpy = jest.spyOn(service, 'searchRecipes').mockResolvedValue(recipeDocument);

      const result = await resolver.searchRecipe(query);

      expect(result).toEqual(mockRecipes);
      expect(searchRecipesSpy).toHaveBeenCalledWith(query);
    });
  });
});
