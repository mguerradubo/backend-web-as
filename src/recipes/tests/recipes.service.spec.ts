import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { Recipe } from '../recipe.schema';
import { IngredientsService } from '../../ingredients/ingredients.service';
import { CategoriesService } from '../../categories/categories.service';
import { getModelToken } from '@nestjs/mongoose';
import { RecipesService } from '../recipes.service';

describe('RecipesService', () => {
  let service: RecipesService;
  let recipeModel: Model<Recipe>
  let ingredientService: IngredientsService;
  let RecipeService: CategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RecipesService,
        {
          provide: getModelToken(Recipe.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: IngredientsService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: CategoriesService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<RecipesService>(RecipesService);
    recipeModel = module.get<Model<Recipe>>(getModelToken(Recipe.name));
    ingredientService = module.get<IngredientsService>(IngredientsService);
    RecipeService = module.get<CategoriesService>(CategoriesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of recipes', async () => {
      const mockRecipe = [{ name: 'Recipe 1', Recipe: 'Recipe 1', instruction: 'instruction'}, { name: 'recipe 2', Recipe: 'Recipe 1', instruction: 'instruction' }];
      recipeModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockRecipe),
      });

      const result = await service.findAll();

      expect(result).toEqual(mockRecipe);
    });

    it('should handle errors and reject the promise on failure', async () => {
      recipeModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findAll()).rejects.toThrow('Test error');
    });
  });

  describe('findOne', () => {
    it('should find a recipe by ID', async () => {
      const mockRecipe = {_id: 'someRecipeId',  name: 'Recipe 1', Recipe: 'Recipe 1', instruction: 'instruction'};
      recipeModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockRecipe),
      });

      const result = await service.findOne('someRecipeId');

      expect(result).toEqual(mockRecipe);
    });

    it('should handle errors and reject the promise on failure', async () => {
      // Mock the findOne method of the recipeModel to throw an error
      recipeModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findOne('someRecipeId')).rejects.toThrow('Test error');
    });
  });

  describe('findCategoryRecipes', () => {
    it('should return recipes of a specific category', async () => {
      const category = 'SomeCategory';
      const mockRecipes = [{ name: 'Recipe 1' }, { name: 'Recipe 2' }];
      recipeModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockRecipes),
      });

      const result = await service.findCategoryRecipes(category);

      expect(result).toEqual(mockRecipes);
      // Asegúrate de que se haya llamado a recipeModel.find con el argumento correcto (category)
      expect(recipeModel.find).toHaveBeenCalledWith({ category });
    });
  });

  describe('searchRecipes', () => {
    it('should return recipes matching a query', async () => {
      const query = 'SearchQuery';
      const mockRecipes = [{ name: 'Recipe 1' }, { name: 'Recipe 2' }];
      recipeModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockRecipes),
      });

      const result = await service.searchRecipes(query);

      expect(result).toEqual(mockRecipes);
      // Asegúrate de que se haya llamado a recipeModel.find con la consulta correcta
      expect(recipeModel.find).toHaveBeenCalledWith({
        name: { $regex: query, $options: 'i' },
      });
    });

    it('should handle errors and reject the promise on failure', async () => {
      const query = 'SearchQuery';
      recipeModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Error en la búsqueda de recetas')),
      });

      await expect(service.searchRecipes(query)).rejects.toThrow('Error en la búsqueda de recetas');
    });
  });
});
