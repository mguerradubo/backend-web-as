import { Resolver, Query, Mutation, Args, ResolveField, Parent } from '@nestjs/graphql';
import { RecipesService } from './recipes.service';
import { CreateRecipeInput } from './dto/create-recipe.input';
import { UpdateRecipeInput } from './dto/update-recipe.input';
import { Recipe } from './recipe.schema';
import { Ingredient } from '../ingredients/ingredient.schema';
import { Comment } from '../comments/comment.schema';
@Resolver('Recipe')
export class RecipesResolver {
  constructor(private readonly recipesService: RecipesService) {}

  @Mutation('loadRecipes')
  loadRecipes(@Args('letter') letter: string): Promise<Boolean>{
    return this.recipesService.loadRecipes(letter);
  }

  @Query('recipes')
  findAll(): Promise<Recipe[]>{
    return this.recipesService.findAll();
  }

  @Query('recipe')
  findOne(@Args('id') id: string): Promise<Recipe> {
    return this.recipesService.findOne(id);
  }

  @Query('random_recipe')
  findRandom(@Args('limit') limit: number): Promise<Recipe[]>{
    return this.recipesService.findRandom(limit)
  }

  @Query(() => [Recipe])
  category_recipes(@Args('category') category: string): Promise<Recipe[]>{
    return this.recipesService.findCategoryRecipes(category)
  }

  @Query()
  searchRecipe(@Args('query') query: string){
    return this.recipesService.searchRecipes(query)
  }

 
}
