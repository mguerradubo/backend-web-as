import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { Field, ObjectType } from '@nestjs/graphql';
import * as mongoose from 'mongoose';
import { Ingredient } from "../ingredients/ingredient.schema";

export type RecipeDocument = HydratedDocument<Recipe>

@ObjectType()
@Schema()
export class Recipe {
    @Field()
    _id: string

    @Field()
    @Prop({unique: true, type: String, required: true, index: 'text' })
    name: string;

    @Field()
    @Prop()
    category: string;

    @Field()
    @Prop()
    instruction: string;

    @Field(()=> Ingredient)
    @Prop([{type: mongoose.Schema}])
    ingredients: [Ingredient];

    @Field({nullable: true})
    @Prop([String])
    measure: string[];

    @Field({nullable: true})
    @Prop()
    image: string;

    @Field({nullable: true})
    @Prop()
    video: string;

    @Field(() => [Comment])
    @Prop({ type: mongoose.Schema, ref: 'Comment' })
    comments: [Comment];

}
export const RecipeSchema = SchemaFactory.createForClass(Recipe);