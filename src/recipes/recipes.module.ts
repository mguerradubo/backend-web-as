import { Module } from '@nestjs/common';
import { RecipesService } from './recipes.service';
import { RecipesResolver } from './recipes.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { Recipe, RecipeSchema } from './recipe.schema';
import { IngredientsModule } from '../ingredients/ingredients.module';
import { CategoriesModule } from '../categories/categories.module';
import { CommentsModule } from '../comments/comments.module';

@Module({
  imports: [MongooseModule.forFeature([{name: Recipe.name, schema: RecipeSchema}]), IngredientsModule, CategoriesModule],
  providers: [RecipesResolver, RecipesService],
  exports: [RecipesService]
})
export class RecipesModule {}
