import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Recipe } from './recipe.schema';
import { Model } from 'mongoose';
import axios from 'axios';
import { IngredientsService } from '../ingredients/ingredients.service';
import { CategoriesService } from '../categories/categories.service';
import { Comment } from '../comments/comment.schema';
import { Category } from '../categories/category.schema';

@Injectable()
export class RecipesService {
  constructor(
    @InjectModel(Recipe.name) private recipeModel: Model<Recipe>,
    private ingredientModel: IngredientsService,
    private categoryModel: CategoriesService,
    ){}
    async loadRecipes(letter: string): Promise<boolean> {
      try {
        const response = await axios.get(`https://www.themealdb.com/api/json/v1/1/search.php?f=${letter}`);
        const recipesData = response.data.meals;
    
        for (const data of recipesData) {
          const ingredients = [];
          const measures = [];
          const category = await this.categoryModel.findByName(data.strCategory);
          // Dynamically create ingredients and measures arrays
          for (let i = 1; i <= 20; i++) {
            const ingredient = data[`strIngredient${i}`];
            const measure = data[`strMeasure${i}`];
    
            if (ingredient && measure) {
              const capitalizedIngredientName = ingredient.charAt(0).toUpperCase() + ingredient.slice(1);

              const ingredientObj = await this.ingredientModel.findByName(capitalizedIngredientName);
    
              if (category && ingredientObj) {
                ingredients.push(ingredientObj);
                measures.push(measure);
              }
            }
          }
    
          const recipe = new this.recipeModel({
            name: data.strMeal,
            category: category.name, // Usar el objeto completo de categoría
            instruction: data.strInstructions,
            image: data.strMealThumb || '',
            video: data.strYoutube || '',
            ingredients: ingredients, // Usar los objetos completos de ingredientes
            measure: measures,
          });
    
          await recipe.save();
        }
        return true;
      } catch (error) {
        console.log('Error loading recipes', error);
        return false;
      }
    }
    


  async findAll(): Promise<Recipe[]>{
    return this.recipeModel.find().exec();
  }

  findOne(id: string): Promise<Recipe> {
    return  this.recipeModel.findOne({_id: id}).exec();
  }

  async findRandom(limit: number): Promise<Recipe[]> {
    const totalRecipes = await this.recipeModel.countDocuments();
    const randomSkip = Math.floor(Math.random() * totalRecipes);
  
    return this.recipeModel
      .find()
      .skip(randomSkip)
      .limit(limit)
      .exec();
  }

  async findCategoryRecipes(category: string): Promise<Recipe[]> {
    return this.recipeModel.find({category}).exec();
}
  
  async searchRecipes(query: string): Promise<Recipe[]> {
    try {
      
      const result = await this.recipeModel
        .find({ name: { $regex: query, $options: 'i' } }) // La opción 'i' hace que la búsqueda sea insensible a mayúsculas y minúsculas.
        .exec();
  
      return result;
    } catch (error) {
      console.error('Error en la búsqueda de recetas:', error);
      throw new Error('Error en la búsqueda de recetas');
    }
  }
  async updateRecipeWithComment(recipeId: string, comments: Comment): Promise<Recipe> {
    const updatedRecipe = await this.recipeModel.findByIdAndUpdate(
      recipeId,
      { $push: { comments } },
      { new: true } // Devuelve el usuario actualizado
    );

    return updatedRecipe;
  }

  async updateRecipeWithoutComment(recipeId: string, comments: Comment): Promise<Recipe> {
    // Encuentra el usuario por su _id y actualiza el campo "comments" para eliminar la referencia al comentario
    const updatedUser = await this.recipeModel.findByIdAndUpdate(
      recipeId,
      { $pull: { comments } }, // Utiliza $pull para eliminar el comentario del arreglo
      { new: true } // Devuelve el usuario actualizado
    );
  
    return updatedUser;
  }
  
}