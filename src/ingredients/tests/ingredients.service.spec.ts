import { Test, TestingModule } from '@nestjs/testing';
import { IngredientsService } from '../ingredients.service';
import { getModelToken } from '@nestjs/mongoose';
import { Ingredient } from '../ingredient.schema';
import { Model } from 'mongoose';

describe('IngredientsService', () => {
  let service: IngredientsService;
  let ingredientModel: Model<Ingredient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IngredientsService,
        {
          provide: getModelToken(Ingredient.name),
          useValue: {
            findOne: jest.fn(),
            find: jest.fn(),
            countDocuments: jest.fn().mockResolvedValue(10),
          },
        },
      ],
    }).compile();

    service = module.get<IngredientsService>(IngredientsService);
    ingredientModel = module.get<Model<Ingredient>>(getModelToken(Ingredient.name));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of ingredient', async () => {
      const mockCategories = [{ name: 'Ingredient 1' }, { name: 'Ingredient 2' }];
      ingredientModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategories),
      });

      const result = await service.findAll();

      expect(result).toEqual(mockCategories);
    });

    it('should handle errors and reject the promise on failure', async () => {
      ingredientModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findAll()).rejects.toThrow('Test error');
    });
  });

  describe('findOne', () => {
    it('should find a ingredient by ID', async () => {
      const mockCategory = {_id: 'someIngredientId', name: 'Ingredient 1' };
      ingredientModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategory),
      });

      const result = await service.findOne('someIngredientId');

      expect(result).toEqual(mockCategory);
    });

    it('should handle errors and reject the promise on failure', async () => {
      // Mock the findOne method of the ingredientModel to throw an error
      ingredientModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findOne('someIngredientId')).rejects.toThrow('Test error');
    });
  });

  describe('findByName', () => {
    it('should find a category by name', async () => {
      const ingredientName = 'Ingredient 1';
      const mockCategory = { _id: 'someIngredientId', name: ingredientName };
      ingredientModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategory),
      });

      const result = await service.findByName(ingredientName);
      expect(result).toEqual(mockCategory);
    });

    it('should handle errors and reject the promise on failure', async () => {
      // Mock the findOne method of the ingredientModel to throw an error
      ingredientModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findByName('Ingredient 1')).rejects.toThrow('Test error');
    });
  });

     
});
