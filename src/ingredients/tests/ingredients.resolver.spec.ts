import { Test, TestingModule } from '@nestjs/testing';
import { IngredientsResolver } from '../ingredients.resolver';
import { IngredientsService } from '../ingredients.service';
import { getModelToken } from '@nestjs/mongoose';
import { Ingredient } from '../ingredient.schema';

describe('IngredientsResolver', () => {
  let resolver: IngredientsResolver;
  let service: IngredientsService

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [IngredientsResolver, IngredientsService,
        {
          provide: getModelToken(Ingredient.name),
          useValue: {
            find: jest.fn(),
          },
        }
      ],
    }).compile();

    resolver = module.get<IngredientsResolver>(IngredientsResolver);
    service = module.get<IngredientsService>(IngredientsService);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  describe('findAll', () => {
    it('should call findAll method of IngredientsService', async () => {
      const ingredients = [{ id: '1', name: 'Ingredient 1' }, { id: '2', name: 'Ingredient 2' }];
      service.findAll = jest.fn().mockResolvedValue(ingredients);

      const result = await resolver.findAll();

      expect(result).toEqual(ingredients);
      expect(service.findAll).toHaveBeenCalled();
    });
  });

  describe('findOne', () => {
    it('should call findOne method of IngredientsService with the provided ID', async () => {
      const ingredientId = 'ingredientId';
      const ingredient = { id: ingredientId, name: 'Ingredient 1' };
      const ingredientDocument = ingredient as any;
      
      const findOneSpy = jest.spyOn(service, 'findOne').mockResolvedValue(ingredientDocument);
  
      const result = await resolver.findOne(ingredientId);
  
      expect(result).toEqual(ingredientDocument);
      expect(findOneSpy).toHaveBeenCalledWith(ingredientId);
    });
  });
  

  describe('findRandom', () => {
    it('should call findRandom method of IngredientsService with the provided limit', async () => {
      const limit = 2;
      const randomIngredients = [{ id: '1', name: 'Ingredient 1' }, { id: '2', name: 'Ingredient 2' }];
      service.findRandom = jest.fn().mockResolvedValue(randomIngredients);

      const result = await resolver.findRandom(limit);

      expect(result).toEqual(randomIngredients);
      expect(service.findRandom).toHaveBeenCalledWith(limit);
    });
  });
});
