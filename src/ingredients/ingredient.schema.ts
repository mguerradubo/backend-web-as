import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { Field, ObjectType } from '@nestjs/graphql';

export type IngredientDocument = HydratedDocument<Ingredient>

@ObjectType()
@Schema()
export class Ingredient {
    @Field()
    @Prop({unique: true, type: String, required: true, index: 'text' })
    name: string;

    @Field({nullable: true})
    @Prop({default: ''})
    description: string;

    @Field()
    @Prop()
    image: string;
}
export const IngredientSchema = SchemaFactory.createForClass(Ingredient);
IngredientSchema.virtual('recipes',{
    ref: 'Recipe',
    localField: 'name',
    foreignField: 'ingredients'
})