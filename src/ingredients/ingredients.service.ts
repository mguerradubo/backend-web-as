import { Injectable } from '@nestjs/common';
import { CreateIngredientInput } from './dto/create-ingredient.input';
import { UpdateIngredientInput } from './dto/update-ingredient.input';
import { Ingredient } from './ingredient.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import axios from 'axios';

@Injectable()
export class IngredientsService {
  constructor(@InjectModel(Ingredient.name) private ingredientModel: Model<Ingredient>){}

  async loadIngredients(): Promise<boolean>{
    try{
      const response = await axios.get('https://www.themealdb.com/api/json/v1/1/list.php?i=list');
      const ingredientData = response.data.meals;
      await Promise.all(
        ingredientData.map(async (data) => {
          const ingredient = new this.ingredientModel({
            name: data.strIngredient,
            description: data.strDescription || '',
            image: `https://www.themealdb.com/images/ingredients/${data.strIngredient}.png`
          })
          await ingredient.save()
        })
      );
      return true;
    }
    catch(error){
      console.log('Error loading ingredients', error);
      return false;
    }
  }

  findAll(): Promise<Ingredient[]>{
    return this.ingredientModel.find().exec();
  }

  async findOne(id: string):Promise<Ingredient> {
    return this.ingredientModel.findOne({_id: id}).exec();;
  }

  findByName(name: string):Promise<Ingredient> {
    return this.ingredientModel.findOne({name: name}).exec();;
  }
  
  async findRandom(limit: number): Promise<Ingredient[]> {
    const totalIngredient = await this.ingredientModel.countDocuments();
    const randomSkip = Math.floor(Math.random() * totalIngredient);
  
    return this.ingredientModel
      .find()
      .skip(randomSkip)
      .limit(limit)
      .exec();
  }
}
