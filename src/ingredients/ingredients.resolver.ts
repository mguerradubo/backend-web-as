import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { IngredientsService } from './ingredients.service';
import { CreateIngredientInput } from './dto/create-ingredient.input';
import { UpdateIngredientInput } from './dto/update-ingredient.input';
import { Ingredient } from './ingredient.schema';

@Resolver('Ingredient')
export class IngredientsResolver {
  constructor(private readonly ingredientsService: IngredientsService) {}

  @Mutation('loadIngredients')
  loadCategories(): Promise<Boolean>{
    return this.ingredientsService.loadIngredients();
  }

  @Query('ingredients')
  findAll(): Promise<Ingredient[]> {
    return this.ingredientsService.findAll();
  }

  @Query('ingredient')
  findOne(@Args('id') id: string): Promise<Ingredient> {
    return this.ingredientsService.findOne(id);
  }

  @Query('random_ingredient')
  findRandom(@Args('limit') limit: number): Promise<Ingredient[]>{
    return this.ingredientsService.findRandom(limit)
  }

}
