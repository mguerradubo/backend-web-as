import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { IngredientsModule } from './ingredients/ingredients.module';
import { CategoriesModule } from './categories/categories.module';
import { RecipesModule } from './recipes/recipes.module';
import { UsersModule } from './users/users.module';
import { FavoritesModule } from './favorites/favorites.module';
import { CommentsModule } from './comments/comments.module';
import * as dotenv from 'dotenv';

dotenv.config()
@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      installSubscriptionHandlers: true,
      typePaths: ['./**/*.graphql'],
      context: ({req}) => ({ headers: req.headers}),
    }),
    MongooseModule.forRoot(
      process.env.URI,
      {autoIndex: true}
    ),
    IngredientsModule,
    CategoriesModule,
    RecipesModule,
    UsersModule,
    FavoritesModule,
    CommentsModule,
  ],
  controllers: [],
  providers: [],

})
export class AppModule {}