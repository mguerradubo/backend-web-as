import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { FavoritesService } from './favorites.service';
import { CreateFavoriteInput } from './dto/create-favorite.input';
import { UpdateFavoriteInput } from './dto/update-favorite.input';
import { Favorite } from './favorite.schema';

@Resolver('Favorite')
export class FavoritesResolver {
  constructor(private readonly favoritesService: FavoritesService) {}

  @Mutation(() => Favorite)
  addFavorite(@Args('input') input: CreateFavoriteInput) {
    return this.favoritesService.createFavorite(input.name, input.email);
  }

  @Query('favorites')
  findAll() {
    return this.favoritesService.findAll();
  }

  @Query('favorite')
  findOne(@Args('id') id: string) {
    return this.favoritesService.findOne(id);
  }

  @Mutation('updateFavorite')
  update(@Args('input') input: UpdateFavoriteInput) {
    return this.favoritesService.update(input.id, input);
  }

  @Mutation('removeFavorite')
  remove(@Args('id') id: number) {
    return this.favoritesService.remove(id);
  }
}
