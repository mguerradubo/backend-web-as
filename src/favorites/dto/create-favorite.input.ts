import { Field, InputType } from "@nestjs/graphql";
import { IsNotEmpty, IsEmail} from 'class-validator';

@InputType()
export class CreateFavoriteInput {
    @IsEmail()
    @Field()
    email: string;

    @IsNotEmpty()
    @Field()
    name: string;
}
