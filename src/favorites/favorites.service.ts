import { Injectable } from '@nestjs/common';
import { CreateFavoriteInput } from './dto/create-favorite.input';
import { UpdateFavoriteInput } from './dto/update-favorite.input';
import { InjectModel } from '@nestjs/mongoose';
import { Favorite } from './favorite.schema';
import { Model } from 'mongoose';
import { UsersService } from '../users/users.service';
import { RecipesService } from '../recipes/recipes.service';

@Injectable()
export class FavoritesService {
  constructor(
    @InjectModel(Favorite.name) private favoriteModel: Model<Favorite>,
    private userModel: UsersService,
    private recipeModel: RecipesService,
  ){}
  async createFavorite(name: string, email: string): Promise<Favorite> {
    const user = await this.userModel.getUserByEmail(email);
    const favorite = new this.favoriteModel({user: user._id, name});
    await favorite.save();
    await this.userModel.updateUserWithFavorite(user._id, favorite);
    return favorite;
  }

  async findAll(): Promise<Favorite[]> {
    return this.favoriteModel.find().exec();
  }

  async findOne(id: string): Promise<Favorite> {
    return this.favoriteModel.findOne({_id: id}).exec();
  }

  update(id: number, updateFavoriteInput: UpdateFavoriteInput) {
    return `This action updates a #${id} favorite`;
  }

  remove(id: number) {
    return `This action removes a #${id} favorite`;
  }
}
