import { Test, TestingModule } from '@nestjs/testing';
import { FavoritesResolver } from '../favorites.resolver';
import { FavoritesService } from '../favorites.service';
import { getModelToken } from '@nestjs/mongoose';
import { Favorite } from '../favorite.schema';
import { UsersService } from '../../users/users.service';
import { RecipesService } from '../../recipes/recipes.service';

describe('FavoritesResolver', () => {
  let resolver: FavoritesResolver;
  let service: FavoritesService;
  let usersService: UsersService;
  let recipesService: RecipesService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FavoritesResolver, FavoritesService,
        {
          provide: getModelToken(Favorite.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: UsersService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: RecipesService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    resolver = module.get<FavoritesResolver>(FavoritesResolver);
    service = module.get<FavoritesService>(FavoritesService);
    usersService = module.get<UsersService>(UsersService);
    recipesService = module.get<RecipesService>(RecipesService);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
