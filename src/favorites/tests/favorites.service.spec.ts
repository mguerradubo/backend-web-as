import { Test, TestingModule } from '@nestjs/testing';
import { FavoritesService } from '../favorites.service';
import { Model } from 'mongoose';
import { Favorite } from '../favorite.schema';
import { getModelToken } from '@nestjs/mongoose';
import { UsersService } from '../../users/users.service';
import { RecipesService } from '../../recipes/recipes.service';

describe('FavoritesService', () => {
  let service: FavoritesService;
  let favoriteModel: Model<Favorite>;
  let usersService: UsersService;
  let recipesService: RecipesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FavoritesService,
        {
          provide: getModelToken(Favorite.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: UsersService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: RecipesService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<FavoritesService>(FavoritesService);
    favoriteModel = module.get<Model<Favorite>>(getModelToken(Favorite.name));
    usersService = module.get<UsersService>(UsersService);
    recipesService = module.get<RecipesService>(RecipesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
