import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import * as mongoose from "mongoose";
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from "../users/user.schema";
import { Recipe } from "../recipes/recipe.schema";

export type FavoriteDocument = HydratedDocument<Favorite>;

@ObjectType()
@Schema()
export class Favorite {
    @Field()
    _id: string;

    @Field()
    @Prop()
    name: string;
    
    @Field()
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User'})
    user: string;

    @Field(() => [Recipe])
    @Prop({ type: mongoose.Schema})
    recipe?: [Recipe];

}

export const favoriteSchema = SchemaFactory.createForClass(Favorite);
