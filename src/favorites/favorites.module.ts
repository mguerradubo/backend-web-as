import { Module } from '@nestjs/common';
import { FavoritesService } from './favorites.service';
import { FavoritesResolver } from './favorites.resolver';
import { MongooseModule } from '@nestjs/mongoose';
import { Favorite, favoriteSchema } from './favorite.schema';
import { UsersModule } from '../users/users.module';
import { RecipesModule } from '../recipes/recipes.module';

@Module({
  imports: [MongooseModule.forFeature([{name: Favorite.name, schema: favoriteSchema}]), UsersModule, RecipesModule],
  providers: [FavoritesResolver, FavoritesService],
})
export class FavoritesModule {}
