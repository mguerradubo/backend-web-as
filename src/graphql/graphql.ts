
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class CreateCategoryInput {
    exampleField?: Nullable<number>;
}

export class UpdateCategoryInput {
    id: number;
}

export class CreateCommentinput {
    text: string;
    recipeID: string;
    email: string;
}

export class CreateFavoriteInput {
    email: string;
    name: string;
}

export class UpdateFavoriteInput {
    id: number;
}

export class CreateIngredientInput {
    exampleField?: Nullable<number>;
}

export class UpdateIngredientInput {
    id: number;
}

export class CreateRecipeInput {
    exampleField?: Nullable<number>;
}

export class UpdateRecipeInput {
    id: number;
}

export class CreateUserInput {
    email: string;
    name: string;
    lastname: string;
    password: string;
}

export class LoginUserInput {
    email: string;
    password: string;
}

export class UpdateUserInput {
    id: number;
}

export class Category {
    _id: string;
    name: string;
}

export abstract class IQuery {
    abstract categories(): Nullable<Category>[] | Promise<Nullable<Category>[]>;

    abstract category(id: string): Nullable<Category> | Promise<Nullable<Category>>;

    abstract comments(): Nullable<Comment>[] | Promise<Nullable<Comment>[]>;

    abstract favorites(): Nullable<Favorite>[] | Promise<Nullable<Favorite>[]>;

    abstract favorite(id: string): Nullable<Favorite> | Promise<Nullable<Favorite>>;

    abstract ingredients(): Nullable<Ingredient>[] | Promise<Nullable<Ingredient>[]>;

    abstract ingredient(id: string): Nullable<Ingredient> | Promise<Nullable<Ingredient>>;

    abstract random_ingredient(limit: number): Nullable<Nullable<Ingredient>[]> | Promise<Nullable<Nullable<Ingredient>[]>>;

    abstract recipes(): Nullable<Recipe>[] | Promise<Nullable<Recipe>[]>;

    abstract recipe(id: string): Nullable<Recipe> | Promise<Nullable<Recipe>>;

    abstract random_recipe(limit: number): Nullable<Nullable<Recipe>[]> | Promise<Nullable<Nullable<Recipe>[]>>;

    abstract category_recipes(category: string): Nullable<Nullable<Recipe>[]> | Promise<Nullable<Nullable<Recipe>[]>>;

    abstract searchRecipe(query: string): Nullable<Nullable<Recipe>[]> | Promise<Nullable<Nullable<Recipe>[]>>;

    abstract user(email: string): Nullable<User> | Promise<Nullable<User>>;
}

export abstract class IMutation {
    abstract loadCategories(): boolean | Promise<boolean>;

    abstract addComment(input?: Nullable<CreateCommentinput>): Nullable<Comment> | Promise<Nullable<Comment>>;

    abstract removeComment(id: string): Nullable<boolean> | Promise<Nullable<boolean>>;

    abstract addFavorite(input: CreateFavoriteInput): Favorite | Promise<Favorite>;

    abstract updateFavorite(input: UpdateFavoriteInput): Favorite | Promise<Favorite>;

    abstract removeFavorite(id: number): Nullable<Favorite> | Promise<Nullable<Favorite>>;

    abstract loadIngredients(): boolean | Promise<boolean>;

    abstract loadRecipes(letter: string): boolean | Promise<boolean>;

    abstract login(input?: Nullable<LoginUserInput>): Nullable<string> | Promise<Nullable<string>>;

    abstract signUp(input?: Nullable<CreateUserInput>): Nullable<string> | Promise<Nullable<string>>;
}

export class Comment {
    _id: string;
    text: string;
    user: User;
    recipe: Recipe;
    createdAt: string;
}

export abstract class ISubscription {
    abstract commentAdded(recipeId: string): Nullable<Comment> | Promise<Nullable<Comment>>;
}

export class Favorite {
    _id: string;
    name: string;
    user: User;
    recipe?: Nullable<Nullable<Recipe>[]>;
}

export class Ingredient {
    _id: string;
    name: string;
    description: string;
    image: string;
}

export class Recipe {
    _id: string;
    name: string;
    category: string;
    instruction: string;
    ingredients: Ingredient[];
    measure?: Nullable<string[]>;
    image: string;
    video: string;
    comments?: Nullable<Nullable<Comment>[]>;
}

export class User {
    _id: string;
    email: string;
    name: string;
    lastname: string;
    password: string;
    comments?: Nullable<Nullable<Comment>[]>;
    favorites?: Nullable<Nullable<Favorite>[]>;
}

type Nullable<T> = T | null;
