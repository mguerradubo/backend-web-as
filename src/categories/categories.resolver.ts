import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { CategoriesService } from './categories.service';
import { Category } from './category.schema';

@Resolver('Category')
export class CategoriesResolver {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Mutation('loadCategories')
  loadCategories(): Promise<Boolean>{
    return this.categoriesService.loadCategory();
  }
  
  @Query('categories')
  findAll(): Promise<Category[]>{
    return this.categoriesService.findAll();
  }

  @Query('category')
  findOne(@Args('id') id: string): Promise<Category> {
    return this.categoriesService.findOne(id);
  }
}
