import { Test, TestingModule } from '@nestjs/testing';
import { CategoriesService } from '../categories.service';
import { MongooseModule, getModelToken} from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Category } from '../category.schema';
import * as dotenv from 'dotenv';

dotenv.config()
describe('CategoriesService', () => {
  let categoriesService: CategoriesService;
  let categoryModel: Model<Category>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CategoriesService,
        {
          provide: getModelToken(Category.name),
          useValue: {
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    categoriesService = module.get<CategoriesService>(CategoriesService);
    categoryModel = module.get<Model<Category>>(getModelToken(Category.name));
  });

  it('should be defined', () => {
    expect(categoriesService).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of categories', async () => {
      const mockCategories = [{ name: 'Category 1' }, { name: 'Category 2' }];
      categoryModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategories),
      });

      const result = await categoriesService.findAll();

      expect(result).toEqual(mockCategories);
    });

    it('should handle errors and reject the promise on failure', async () => {
      categoryModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(categoriesService.findAll()).rejects.toThrow('Test error');
    });
  });

  describe('findOne', () => {
    it('should find a category by ID', async () => {
      const mockCategory = {_id: 'someCategoryId', name: 'Category 1' };
      categoryModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategory),
      });

      const result = await categoriesService.findOne('someCategoryId');

      expect(result).toEqual(mockCategory);
    });

    it('should handle errors and reject the promise on failure', async () => {
      // Mock the findOne method of the categoryModel to throw an error
      categoryModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(categoriesService.findOne('someCategoryId')).rejects.toThrow('Test error');
    });
  });

  describe('findByName', () => {
    it('should find a category by name', async () => {
      const categoryName = 'Category 1';
      const mockCategory = { _id: 'someCategoryId', name: categoryName };
      categoryModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategory),
      });

      const result = await categoriesService.findByName(categoryName);
      expect(result).toEqual(mockCategory);
    });

    it('should handle errors and reject the promise on failure', async () => {
      // Mock the findOne method of the categoryModel to throw an error
      categoryModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(categoriesService.findByName('Category 1')).rejects.toThrow('Test error');
    });
  });
});
