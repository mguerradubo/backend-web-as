import { CategoriesResolver } from '../categories.resolver';
import { CategoriesService } from '../categories.service';
import { MongooseModule, getModelToken} from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Category } from '../category.schema';

describe('CategoriesResolver', () => {
  let resolver: CategoriesResolver;
  let service: CategoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CategoriesResolver, CategoriesService,
        {
          provide: getModelToken(Category.name),
          useValue: {
            find: jest.fn(),
          },
        },],
    }).compile();

    resolver = module.get<CategoriesResolver>(CategoriesResolver);
    service = module.get<CategoriesService>(CategoriesService);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  describe('findAll', () => {
    it('should call findAll method of the CategoriesService', async () => {
      const categories = [{_id:'1', name: 'Category 1' }, {_id: '2', name: 'Category 2' }];
      const findAllSpy = jest.spyOn(service, 'findAll').mockResolvedValue(categories);

      const result = await resolver.findAll();

      expect(result).toEqual(categories);
      expect(findAllSpy).toHaveBeenCalled();
    });
  });

  describe('findOne', () => {
    it('should call findOne method of the CategoriesService', async () => {
      const categoryId = 'someCategoryId';
      const category = { _id: categoryId, name: 'Category 1' };

      // Convierte el objeto en un documento Mongoose válido
      const categoryDocument = category as any;

      const findOneSpy = jest.spyOn(service, 'findOne').mockResolvedValue(categoryDocument);

      const result = await resolver.findOne(categoryId);

      expect(result).toEqual(categoryDocument);
      expect(findOneSpy).toHaveBeenCalledWith(categoryId);
    });
  });
});
