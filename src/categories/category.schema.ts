import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import { Field, ObjectType } from '@nestjs/graphql';

export type CategoryDocument = HydratedDocument<Category>

@ObjectType()
@Schema()
export class Category {
    @Field()
    _id: string;
    
    @Field({nullable: true})
    @Prop({unique: true})
    name: string;
}
export const CategorySchema = SchemaFactory.createForClass(Category);
CategorySchema.virtual('recipes',{
    ref: 'Recipe',
    localField: 'name',
    foreignField: 'category'
})