import { Injectable } from '@nestjs/common';
import { CreateCategoryInput } from './dto/create-category.input';
import { UpdateCategoryInput } from './dto/update-category.input';
import { InjectModel } from '@nestjs/mongoose';
import { Category } from './category.schema';
import { Model } from 'mongoose';
import axios from 'axios';

@Injectable()
export class CategoriesService {
  constructor (@InjectModel(Category.name) private categoryModel: Model<Category>){}

  async loadCategory(): Promise<boolean>{
    try{
      const response = await axios.get('https://www.themealdb.com/api/json/v1/1/list.php?c=list');
      const categoryData = response.data.meals;
      await Promise.all(
        categoryData.map(async (data) => {
          const category = new this.categoryModel({
            name: data.strCategory,
          })
          await category.save()
        })
      );
      return true;
    }
    catch(error){
      console.log('Error loading categories', error);
      return false;
    }
    
  }

  findAll(): Promise<Category[]>{
    return this.categoryModel.find().exec();
  }

  findOne(id: string) {
    return this.categoryModel.findOne({_id: id}).exec();;
  }

  async findByName(name: string): Promise<Category>{
    return this.categoryModel.findOne({name}).exec()
  }

}
