import { Resolver, Query, Mutation, Args, Context } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { CreateUserInput } from './dto/create-user.input';
import { User } from './user.schema';
import { AuthGuard } from './auth.guard';
import { UseGuards } from '@nestjs/common';
import { LoginUserInput } from './dto/login-user.input';

@Resolver('User')
export class UsersResolver {
  constructor(private readonly userService: UsersService) {}

  @Query()
  @UseGuards(new AuthGuard())
  user(@Args('email')email: string): Promise<User> {
    return this.userService.getUserByEmail(email);
  }

  @Mutation()
  async signUp(@Args('input') input: CreateUserInput) {
    const user = await this.userService.getUserByEmail(input.email);
    if (!user) {
      const user = await this.userService.createUser(input);
      return this.userService.createToken(user);
    }
  }

  @Mutation()
  async login(@Args('input') input: LoginUserInput) {
    const user = await this.userService.getUserByEmail(input.email);
    if (user) {
      return this.userService.createToken(user);
    }
  }
}
