import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { HydratedDocument } from "mongoose";
import { Field, ObjectType } from '@nestjs/graphql';
import { Recipe } from "../recipes/recipe.schema";
import { Favorite } from "../favorites/favorite.schema";

export type UserDocument = HydratedDocument<User>

@ObjectType()
@Schema()
export class User {
    @Field()
    _id: string;

    @Field()
    @Prop()
    name: string;

    @Field()
    @Prop()
    lastname: string;

    @Field()
    @Prop({unique: true})
    email: string;

    @Field()
    @Prop()
    password: string;

    @Field(() => [Comment])
    @Prop({ type: mongoose.Schema, ref: 'Comment' })
    comments: [Comment];

    @Field(() => [Favorite])
    @Prop([{type: mongoose.Schema, ref: 'Favorite'}])
    favorites?: [Favorite]

}
export const UserSchema = SchemaFactory.createForClass(User);