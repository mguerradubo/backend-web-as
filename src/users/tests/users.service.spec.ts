import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from '../users.service';
import { User } from '../user.schema';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserInput } from '../dto/create-user.input';

describe('UsersService', () => {
  let service: UsersService;
  let userModel: Model<User>

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService,
        {
          provide: getModelToken(User.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    userModel =  module.get<Model<User>>(getModelToken(User.name));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('createToken', () => {
    it('should create a token', () => {
      const userData = { email: 'test@example.com', name: 'Test User' };

      const userDocument = userData as any;
      const token = service.createToken(userDocument);
      
      expect(typeof token).toBe('string');
      // Asegúrate de que el token sea válido utilizando una biblioteca como jsonwebtoken
    });
  });
  describe('createUser', () => {
    it('should create a new user', async () => {
      const createUserInput: CreateUserInput = {
        email: 'test@example.com',
        password: 'testPassword',
        name: 'Test User',
        lastname: 'Test lastname'
      };

      const userData = new User();
      userData._id = '1';
      userData.name = 'Test User';
      userData.lastname = 'Test lastname';
      userData.email = 'test@example.com';
      userData.password = 'testPassword';

      expect(userData._id).toBeDefined()
      expect(userData.name).toBe(createUserInput.name);
      expect(userData.lastname).toBe(createUserInput.lastname);
      expect(userData.email).toBe(createUserInput.email);
      expect(userData.password).toBe(createUserInput.password);
      
    });
  });

  describe('getUserByEmail', () => {
    it('should find a user by email', async () => {
      const userEmail = 'test@example.com';
      const mockUser = { email: userEmail, name: 'Test User' };
  
      const userDocument = mockUser as any;
  
      // Crea un objeto que imite el resultado de findOne
      const findOneResult: any = {
        exec: jest.fn().mockResolvedValue(userDocument),
      };
  
      // Espía el método userModel.findOne y devuelve el objeto simulado
      const findOneSpy = jest.spyOn(userModel, 'findOne').mockReturnValue(findOneResult);
  
      const result = await service.getUserByEmail(userEmail);
  
      expect(result).toEqual(userDocument);
      expect(findOneSpy).toHaveBeenCalledWith({ email: userEmail });
    });
  });
  
  describe('findOne', () => {
    it('should find a User by ID', async () => {
      const mockCategory = {_id: 'someUserID', name: 'User 1' };
      userModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockCategory),
      });

      const result = await service.findOne('someUserID');

      expect(result).toEqual(mockCategory);
    });

    it('should handle errors and reject the promise on failure', async () => {
      // Mock the findOne method of the userModel to throw an error
      userModel.findOne = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findOne('someUserID')).rejects.toThrow('Test error');
    });
  });
});
