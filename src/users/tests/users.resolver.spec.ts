import { Test, TestingModule } from '@nestjs/testing';
import { UsersResolver } from '../users.resolver';
import { UsersService } from '../users.service';
import { getModelToken } from '@nestjs/mongoose';
import { User } from '../user.schema';
import { AuthGuard } from '../auth.guard';
import { LoginUserInput } from '../dto/login-user.input';
import { CreateUserInput } from '../dto/create-user.input';

describe('UsersResolver', () => {
  let resolver: UsersResolver;
  let service: UsersService;
  let guard: AuthGuard;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersResolver, UsersService,
        {
          provide: getModelToken(User.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    resolver = module.get<UsersResolver>(UsersResolver);
    service = module.get<UsersService>(UsersService);
    guard = new AuthGuard();
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('should return a user when authenticated', async () => {
    const userEmail = 'test@example.com';
    const user = { email: userEmail, name: 'Test User' };
    const context = {
      headers: {
        authorization: 'Bearer valid_token_here',
      },
    };
    const userDocument = user as any;
    // Espía el método getUserByEmail de UsersService
    const getUserByEmailSpy = jest.spyOn(service, 'getUserByEmail').mockResolvedValue(userDocument);

    const result = await resolver.user(userEmail);

    expect(result).toEqual(user);
    expect(getUserByEmailSpy).toHaveBeenCalledWith(userEmail);
  });

  it('should throw an error when not authenticated', async () => {
    const userEmail = 'test@example.com';
    const context = {
      headers: {},
    };

    try {
      await resolver.user(userEmail);
      fail('Expected an exception to be thrown');
    } catch (error) {
      expect(error).toBeInstanceOf(Error); // Asegúrate de ajustar esto al tipo de error que lanza tu AuthGuard.
    }
  });

  it('should sign up a new user and return a token when valid credentials are provided', async () => {
    const input: CreateUserInput = {
      email: 'new_user@example.com',
      name: 'New User',
      password: 'password123',
      lastname: 'lastname'
    };
    const user = { ...input, _id: '1' };

    // Espía el método getUserByEmail para simular que el usuario no existe
    jest.spyOn(service, 'getUserByEmail').mockResolvedValue(null);

    const userDocument = user as any;
    // Espía el método createUser de UsersService para simular la creación de un nuevo usuario
    jest.spyOn(service, 'createUser').mockResolvedValue(userDocument);

    // Espía el método createToken de UsersService para simular la creación de un token
    jest.spyOn(service, 'createToken').mockReturnValue('token123');

    const result = await resolver.signUp(input);

    expect(result).toBe('token123');
  });

  it('should return a token when valid login credentials are provided', async () => {
    const input: LoginUserInput = {
      email: 'existing_user@example.com',
      password: 'password123',
    };
    const user = { email: input.email, name: 'Existing User', _id: '2' };
    const userDocument = user as any;
    // Espía el método getUserByEmail para simular que el usuario existe
    jest.spyOn(service, 'getUserByEmail').mockResolvedValue(userDocument);

    // Espía el método createToken de UsersService para simular la creación de un token
    jest.spyOn(service, 'createToken').mockReturnValue('token123');

    const result = await resolver.login(input);

    expect(result).toBe('token123');
  });

});
