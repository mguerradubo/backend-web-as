import { Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './user.schema';
import { Model } from 'mongoose';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcryptjs';
import { Comment } from '../comments/comment.schema';
import { Favorite } from '../favorites/favorite.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>
  ){}

  createToken({email, name}: User) {
    return jwt.sign({ email, name}, 'secret',{expiresIn: '1h'});
  }

  async createUser(user: CreateUserInput): Promise<User> {
    const { password, ...userData } = user;
    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = new this.userModel({
      ...userData,
      password: hashedPassword,
    });

    return newUser.save();
  }

  async getUserByEmail(email: string): Promise<User> {
    return await this.userModel.findOne({ email }).exec();
  }
  async findOne(id: string): Promise<User> {
    return this.userModel.findOne({ _id: id }).exec();
  }
  async updateUserWithComment(userId: string, comments: any): Promise<User> {
    const updatedUser = await this.userModel.findByIdAndUpdate(
      userId,
      { $push: { comments} },
      { new: true } // Devuelve el usuario actualizado
    );

    return updatedUser;
  }

  async updateUserWithoutComment(userId: string, comments: Comment): Promise<User> {
    
    const updatedUser = await this.userModel.findByIdAndUpdate(
      userId,
      { $pull: { comments} }, // Utiliza $pull para eliminar el comentario del arreglo
      { new: true } // Devuelve el usuario actualizado
    );
  
    return updatedUser;
  }

  async updateUserWithFavorite(userId: string, favorites: Favorite): Promise<User> {
    const updatedUser = await this.userModel.findByIdAndUpdate(
      userId,
      { $addToSet: { favorites} },
      { new: true } // Devuelve el usuario actualizado
    );

    return updatedUser;
  }
  
}
