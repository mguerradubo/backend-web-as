import { Test, TestingModule } from '@nestjs/testing';
import { CommentsResolver } from '../comments.resolver';
import { CommentsService } from '../comments.service';
import { getModelToken } from '@nestjs/mongoose';
import { Comment } from '../comment.schema';
import { UsersService } from '../../users/users.service';
import { RecipesService } from '../../recipes/recipes.service';

describe('CommentsResolver', () => {
  let resolver: CommentsResolver;
  let service: CommentsService
  let usersService: UsersService;
  let recipesService: RecipesService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CommentsResolver, CommentsService,
        {
          provide: getModelToken(Comment.name),
          useValue: {
            find: jest.fn(),
          },
        },
        {
          provide: UsersService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: RecipesService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    resolver = module.get<CommentsResolver>(CommentsResolver);
    service = module.get<CommentsService>(CommentsService);
    usersService = module.get<UsersService>(UsersService);
    recipesService = module.get<RecipesService>(RecipesService);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  describe('findAll', () => {
    it('should call findAll method of CommentsService', async () => {
      const comments = [{ _id: '1', text: 'Comment 1' }, { _id: '2', text: 'Comment 2' }];
      service.findAll = jest.fn().mockResolvedValue(comments);

      const result = await resolver.findAll();

      expect(result).toEqual(comments);
      expect(service.findAll).toHaveBeenCalled();
    });
  });

  describe('addComment', () => {
    it('should call createComment method of CommentsService and publish a new comment', async () => {
      const input = {
        email: 'user@example.com',
        recipeID: 'recipeId',
        text: 'A new comment',
      };

      const newComment = { _id: 'newCommentId', text: 'A new comment' };

      service.createComment = jest.fn().mockResolvedValue(newComment);
      const result = await resolver.addComment(input);

      expect(newComment).toBeDefined();
      expect(service.createComment).toHaveBeenCalledWith(input.email, input.recipeID, input.text);

    });
  });

  describe('removeComment', () => {
    it('should call deleteComment method of CommentsService', async () => {
      const commentId = 'commentId';
      service.deleteComment = jest.fn().mockResolvedValue(true);

      const result = await resolver.removeComment(commentId);

      expect(result).toBe(true);
      expect(service.deleteComment).toHaveBeenCalledWith(commentId);
    });
  });
});
