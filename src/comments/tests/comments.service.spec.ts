import { CommentsService } from '../comments.service';
import { Model } from 'mongoose';
import { UsersService } from '../../users/users.service';
import { RecipesService } from '../../recipes/recipes.service';
import { Comment } from '../comment.schema';
import { Test, TestingModule } from '@nestjs/testing';
import { MongooseModule, getModelToken} from '@nestjs/mongoose';

describe('CommentsService', () => {
  let service: CommentsService;
  let commentModel: Model<Comment>;
  let usersService: UsersService;
  let recipesService: RecipesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentsService,
        {
          provide: getModelToken(Comment.name),
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: UsersService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
        {
          provide: RecipesService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
            findByIdAndRemove: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<CommentsService>(CommentsService);
    commentModel = module.get<Model<Comment>>(getModelToken(Comment.name));
    usersService = module.get<UsersService>(UsersService);
    recipesService = module.get<RecipesService>(RecipesService);

  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createComment', () => {
    it('should create a comment', async () => {
      const email = 'user@example.com';
      const recipeId = 'recipeId';
      const text = 'A comment text';
      const user = { _id: 'userId', email: email };
      const recipe = { _id: recipeId};
      const comment = { _id: 'commentId', text, user, recipe };

      const commentM = new Comment();
      commentM._id = 'commentId';
      commentM.text = text;

      expect(commentM._id).toBe(comment._id);
      expect(commentM.text).toBe(comment.text);
      expect(commentM.recipe).toBeUndefined();
      expect(commentM.user).toBeUndefined();
    });
  });
  
  describe('findAll', () => {
    it('should return an array of comments', async () => {
      const mockComment = [{ name: 'Category 1' }, { name: 'Category 2' }];
      commentModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockResolvedValue(mockComment),
      });

      const result = await service.findAll();

      expect(result).toEqual(mockComment);
    });

    it('should handle errors and reject the promise on failure', async () => {
      commentModel.find = jest.fn().mockReturnValue({
        exec: jest.fn().mockRejectedValue(new Error('Test error')),
      });

      await expect(service.findAll()).rejects.toThrow('Test error');
    });
  });
  
});
