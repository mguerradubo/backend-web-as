import { Args, Mutation, Query, Resolver, Subscription} from '@nestjs/graphql';
import { CommentsService } from './comments.service';
import { CreateCommentinput } from './dto/create.comment.input';
import { Comment } from './comment.schema';
import {PubSub} from 'graphql-subscriptions';

@Resolver('Comment')
export class CommentsResolver {
  constructor(private readonly commentsService: CommentsService,   
    ) {}
private pubSub = new PubSub();
  @Query('comments')
  findAll(): Promise<Comment[]>{
    return this.commentsService.findAll()
  }

  @Mutation(() => Comment)
  addComment(@Args('input') input: CreateCommentinput){
    const newComment = this.commentsService.createComment(input.email, input.recipeID, input.text);

    // Publicar la nueva actualización
    return  this.pubSub.publish('commentAdded', { commentAdded: newComment, recipeId: input.recipeID });;
  }

  @Mutation(() => Boolean)
  removeComment(@Args('id') id: string){
    return this.commentsService.deleteComment(id);
  }

  @Subscription(() => Comment, {
    filter: (payload, variables) => {
      return payload.recipeId === variables.recipeId;
    },
  })
  commentAdded(@Args('recipeId') recipeId: string) {
    return this.pubSub.asyncIterator('commentAdded');
  }
}