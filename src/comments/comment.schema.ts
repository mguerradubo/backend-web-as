import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { HydratedDocument } from "mongoose";
import * as mongoose from "mongoose";
import { Field, ObjectType } from '@nestjs/graphql';
import { User } from "../users/user.schema";
import { Recipe } from "../recipes/recipe.schema";

export type CommentDocument = HydratedDocument<Comment>;

@ObjectType()
@Schema()
export class Comment {
    @Field()
    _id: string;
    
    @Field()
    @Prop({ required: true })
    text: string;

    @Field(() => User)
    @Prop({ type: mongoose.Schema, required: true})
    user: User;

    @Field(() => Recipe)
    @Prop({ type: mongoose.Schema, required: true })
    recipe: Recipe;

    @Field()
    @Prop({type: Date, default: Date.now })
    createdAt: string;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
