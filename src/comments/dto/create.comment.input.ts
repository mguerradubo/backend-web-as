import { Field, InputType } from "@nestjs/graphql";
import { IsNotEmpty, IsEmail} from 'class-validator';

@InputType()
export class CreateCommentinput{
    @IsNotEmpty()
    @Field()
    text: string;

    @IsNotEmpty()
    @Field()
    recipeID: string;
    
    @IsEmail()
    @Field()
    email: string;
}