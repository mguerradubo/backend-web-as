import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RecipesService } from '../recipes/recipes.service';
import { UsersService } from '../users/users.service';
import { Comment } from './comment.schema';

@Injectable()
export class CommentsService {
    constructor(
        @InjectModel(Comment.name) private commentModel: Model<Comment>,
        private userModel: UsersService,
        private recipeModel: RecipesService,
    ){}

    async createComment(email: string, recipeId: string, text: string): Promise<Comment> {
        const user = await this.userModel.getUserByEmail(email);
        const recipe = await this.recipeModel.findOne(recipeId);
        const comment = new this.commentModel({ text, user, recipe });
        await comment.save();

        await this.userModel.updateUserWithComment(user._id, comment);
        await this.recipeModel.updateRecipeWithComment(recipeId, comment);
      
        return comment;
      }

      async deleteComment(commentId: string): Promise<boolean> {
        const comment = await this.commentModel.findOne({_id: commentId}).exec();
        const user = (await comment).user._id;
        const recipe = (await comment).recipe._id
      
        if (comment) {
          await this.userModel.updateUserWithoutComment(user, comment);
          await this.recipeModel.updateRecipeWithoutComment(recipe, comment);

          await this.commentModel.findByIdAndRemove(commentId).exec();
          return true; 
        } else {
          return false; 
        }
      }
      
      async findAll(): Promise<Comment[]>{
        return this.commentModel.find().exec()
      }


}
